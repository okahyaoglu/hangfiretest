﻿using System;
using System.Threading.Tasks;
using Hangfire;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(HangFireTest.Startup1))]

namespace HangFireTest
{
    public class Startup1
    {
        public void Configuration(IAppBuilder app)
        {
            GlobalConfiguration.Configuration.UseSqlServerStorage("ClientEntities");
            app.UseHangfireDashboard();
            app.UseHangfireServer();

            Default.Update("appstart"); ;
            BackgroundJob.Schedule(() => Default.Update("after 15 sec"), TimeSpan.FromSeconds(15));
            RecurringJob.AddOrUpdate(() => Default.Update("each min sec"), Cron.Minutely);
        }
    }
}
