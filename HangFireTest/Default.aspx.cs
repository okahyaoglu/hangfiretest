﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HangFireTest
{
    public partial class Default : System.Web.UI.Page
    {
        public static string State = "";

        public static void Update(string from)
        {
            State += String.Format("Ticked from '{0}': {1} <br>", from, DateTime.Now);
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}